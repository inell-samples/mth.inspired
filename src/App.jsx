import { Route, RouterProvider, createBrowserRouter, createRoutesFromElements } from "react-router-dom";
import { MainPage } from "./Components/MainPage/MainPage";
import { ErrorPage } from "./Components/ErrorPage/ErrorPage";
import { Root } from "./Components/Routes/Root";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { fetchNavigation } from "./features/navigationSlice";
import { fetchColors } from "./features/colorsSlice";
import { ProductPage } from "./Components/ProductPage/ProductPage";
import { FavoritePage } from "./Components/FavoritePage/FavoritePage";
import { CartPage } from "./Components/CartPage/CartPage";
import { SearchPage } from "./Components/SearchPage/SearchPage";

const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path="/" element={<Root />}>
            <Route index element={<MainPage />} />
            <Route path="catalog/:gender/:category?" element={<MainPage />} />
            <Route path="product/:id" element={<ProductPage />} />
            <Route path="/cart" element={<CartPage />} />
            <Route path="/favorite" element={<FavoritePage />} />
            <Route path="/search" element={<SearchPage />} />
            <Route path="*" element={<ErrorPage />} />
        </Route>
    )
);

export default function App() {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchNavigation());
        dispatch(fetchColors());
    },[]);

    return (
        <RouterProvider router={router}></RouterProvider>
    );
}
import { useEffect, useRef } from 'react';
import style from './ErrorPage.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useNavigate, useRouteError } from 'react-router-dom';
import { fetchColors } from '../../features/colorsSlice';
import { fetchNavigation } from '../../features/navigationSlice';

export function ErrorPage() {
    const routeError = useRouteError();
    const { status } = useSelector(state => state.statusServer);

    const location = useLocation();
    const navigate = useNavigate();
    const dispatch = useDispatch();

    // Storing time id instead of useState approach
    const timerIdRef = useRef(null);

    useEffect(() => {
        if (status && location.pathname === '/error') {
            navigate('/');
        }
    }, [navigate, status, location]);

    useEffect(() => {
        if (!status && location.pathname === '/error') {
            clearInterval(timerIdRef.current);
            
            timerIdRef.current = setInterval(() => {
                dispatch(fetchColors());
                dispatch(fetchNavigation());
            }, 3000);
        }

        // When getting away from the page, unmount happens and itš needed to clear timer
        return () => {
            clearInterval(timerIdRef.current);
        }
    }, [dispatch, status, location]);

    return (
        <div className={style.error}>
            <h2 className={style.title}>Произошла ошибка при получении данных</h2>
            <p className={style.message}>{routeError?.message || "Unknown"}</p>
        </div>
    );
}
import { useSelector } from 'react-redux';
import style from './Main.module.scss';
import { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

export function Main({ children }) {

    const { status } = useSelector(state => state.statusServer);
    const location = useLocation();
    const navigate = useNavigate();

    useEffect(() => {
        if (!status && location.pathname !== '/error') {
            navigate('/error');
        }
    }, [navigate, status, location]);

    return (
        <div className={style.main}>
            {children}
        </div >
    );
}
import style from './Container.module.scss';
import cn from 'classnames';

export function Container({children, className}) {
    return (
        <div className={cn(style.container, className)}>
            {children}
        </div>
    );
}
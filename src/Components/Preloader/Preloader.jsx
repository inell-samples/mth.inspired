import { Puff } from "react-loader-spinner";

// The way of setting styles as object
const style = {
    display: 'flex',
    justifyContent: 'center',
    padding: '100px 0'
};

export function Preloader() {
    return (
        <div style={style}>
            <Puff
                width={140}
                height={140}
                color='#dadada'
            />
        </div>
    )
}
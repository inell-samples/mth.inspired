import { useEffect, useRef } from "react";
import style from "./Color.module.scss";
import cn from 'classnames';

export function Color({ color, check }) {

    const colorRef = useRef(null);
    useEffect(() => {
        // It is not data-attribure for html-tag, it is css-variable
        colorRef.current.style.setProperty("--data-color", color);
    }, [color]);

    return (
        <li ref={colorRef} className={cn(style.color, check ? style.colorcCheck : "")}/>
    );
}
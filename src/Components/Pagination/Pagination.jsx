import { NavLink, useLocation } from 'react-router-dom';
import style from './Pagination.module.scss';
import { useDispatch, useSelector } from 'react-redux';
import cn from 'classnames';
import { setPage } from '../../features/goodsSlice';
import { useEffect, useState } from 'react';

export function Pagination() {

    const pathname = useLocation().pathname;

    const { page, pages } = useSelector(state => state.goods);
    const [pagePagination, setPagePagination] = useState(1);

    const dispath = useDispatch();

    useEffect(() => {
        setPagePagination(page);
    }, [page]);

    const handlePageChange = (newPage) => {
        setPagePagination(newPage);
    };

    const handlePrevPage = () => {
        if (pagePagination > 1) {
            handlePageChange(pagePagination - 1);
        }
    };

    const handleNextPage = () => {
        if (pagePagination < pages) {
            handlePageChange(pagePagination + 1);
        }
    };

    const renderPaginationItems = () => {
        const paginationItems = [];

        // On the last page show previous 2, otherwise - 1
        const threshold = pagePagination === pages ? 2 : 1;

        let startPage = Math.max(1, pagePagination - threshold);
        let endPage = Math.min(startPage + 2, pages);

        for (let i = startPage; i <= endPage; i++) {
            paginationItems.push(
                <li key={i} className={style.item}>
                    <NavLink
                        to={`${pathname}?page=${i}`}
                        className={cn(style.link, i === pagePagination ? style.linkActive : "")}
                        onClick={() => handlePageChange(i)}>
                        {i}
                    </NavLink>
                </li>
            );
        }

        return paginationItems;
    }

    return (
        (pages > 1) &&
        <div className={style.pagination}>

            <button className={style.arrow} onClick={handlePrevPage} disabled={pagePagination <= 2}>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    {(pagePagination !== 1) && <path d="M14 15.06L10.9096 12L14 8.94L13.0486 8L9 12L13.0486 16L14 15.06Z" fill="currentColor" />}
                </svg>
            </button>

            <ul className={style.list}>{renderPaginationItems()}</ul>

            <button className={style.arrow} onClick={handleNextPage} disabled={pagePagination >= pages - 1 || pages <= 3}>
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    {(pagePagination !== pages) && <path d="M10 15.06L13.0904 12L10 8.94L10.9514 8L15 12L10.9514 16L10 15.06Z" fill="currentColor" />}
                </svg>
            </button>

        </div>
    );
}
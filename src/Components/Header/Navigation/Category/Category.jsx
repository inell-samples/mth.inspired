import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux/es/hooks/useSelector';
import style from './Category.module.scss';
import cn from 'classnames';

export function Category() {

    const { activeGender, categories } = useSelector(state => state.navigation);

    return (
        <ul className={style.category}>
            {categories[activeGender]?.list?.map(item => (
                <li className={style.item} key={item.slug}>
                    <NavLink className={({ isActive }) => cn(style.link, isActive && style.linkActive)} to={`/catalog/${activeGender}/${item.slug}`}>
                        {item.title}
                    </NavLink>
                </li>
            ))}
        </ul>
    );
}
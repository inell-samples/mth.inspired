import { Container } from "../../Layout/Container/Container";
import { Category } from "./Category/Category";
import { Gender } from "./Gender/Gender";

export function Navigation() {

    return (
        <nav>
            <Container>
                <Gender />
                <Category />
            </Container>
        </nav>
    );
}
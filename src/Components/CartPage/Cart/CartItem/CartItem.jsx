import { useDispatch, useSelector } from 'react-redux';
import style from './CartItem.module.scss';
import { API_URL } from '../../../../const';
import cn from 'classnames';
import { Count } from '../../../Count/Count';
import { addToCart, removeFromCart } from '../../../../features/cartSlice';

export function CartItem({ id, color, size, count, goodsList }) {

    const { colorList } = useSelector(state => state.color);
    const item = goodsList.find(item => item.id === id);

    const dispath = useDispatch();

    const handleCountChange = count => {
        dispath(addToCart({ id, color, size, count }));
    }

    const handleRemoveItem = () => {
        dispath(removeFromCart({ id, color, size }));
    }

    return (
        <article className={style.item}>
            <img className={style.image} src={`${API_URL}${item?.pic}`} alt={item?.title} />

            <div className={style.content}>
                <h3 className={style.title}>{item?.title}</h3>
                <p className={style.price}>руб {item?.price}</p>
                <div className={style.vendorCode}>
                    <span className={style.subtitle}>Артикул</span>
                    <span>{id}</span>
                </div>
            </div>

            <div className={style.prop}>
                <div className={style.color}>
                    <p className={cn(style.subtitle, style.colorTitle)}>Цвет</p>
                    <div
                        className={style.colorItem}
                        style={{ "--data-color": colorList?.find(item => item.title === color)?.code }}></div>
                </div>

                <div className={style.size}>
                    <p className={cn(style.subtitle, style.sizeTitle)}>Цвет</p>
                    <div className={style.sizeItem}>{size}</div>
                </div>
            </div>

            <button className={style.del} aria-label='Удалить товар' onClick={handleRemoveItem}></button>

            <Count className={style.count} count={count}
                handleIncrement={() => {
                    handleCountChange(count + 1);
                }}
                handleDecrement={() => {
                    handleCountChange(count - 1);
                }} />
        </article>
    );
}
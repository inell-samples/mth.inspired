import { useEffect, useState } from "react";
import { Cart } from "./Cart/Cart";
import { Order } from "./Order/Order";
import { useDispatch, useSelector } from 'react-redux';
import { fetchAll } from "../../features/goodsSlice";
import { OrderModal } from "./OrderModal/OrderModal";
import { Preloader } from "../Preloader/Preloader";

export function CartPage() {
    const { cartItems, countItems } = useSelector(state => state.cart);
    const { goodsList, status } = useSelector(state => state.goods);
    const dispatch = useDispatch();
    const [count, setCount] = useState(0);
    const { orderStatus } = useSelector(state => state.cart);

    useEffect(() => {
        // Request data only if amount of unique goods is changed
        if (count !== countItems) {
            dispatch(fetchAll({ list: cartItems.map(item => item.id) }));
            setCount(countItems);
        }
    }, [dispatch, countItems, count, cartItems]);

    return status === 'loading' ? <Preloader /> : (
        <>
            <Cart cartItems={cartItems} goodsList={goodsList} />
            {!!cartItems.length && <Order cartItems={cartItems} />}
            {orderStatus === 'success' && <OrderModal />}
        </>
    );
}
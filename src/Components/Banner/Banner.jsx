import { NavLink } from 'react-router-dom';
import { Container } from '../Layout/Container/Container';
import { useMedia } from 'react-use';
import { useEffect, useState } from 'react';
import { API_URL } from '../../const';
import style from './Banner.module.scss';

export function Banner({ data }) {

    const [bg, setBg] = useState("");

    const isMobile = useMedia('(max-width: 540px)');
    const isTablet = useMedia('(max-width: 768px)');
    const isLaptop = useMedia('(max-width: 1024px)');

    useEffect(() => {
        if (!data)
            return;

        if (isMobile) {
            console.log('Мобильное разрешение');
            setBg(`${API_URL}${data.bg.mobile}`);
        } else if (isTablet) {
            console.log('Разрешение планшета');
            setBg(`${API_URL}${data.bg.tablet}`);
        } else if (isLaptop) {
            console.log('Разрешение ноутбука');
            setBg(`${API_URL}${data.bg.laptop}`);
        } else {
            setBg(`${API_URL}${data.bg.desktop}`);
            console.log('Десктопное разрешение');
        }
    }, [data, isMobile, isTablet, isLaptop]);

    return (
        data &&
        <section className={style.banner} style={{
            backgroundImage: `url(${bg})`
        }}>
            <Container>
                <div className={style.content}>
                    <h2 className={style.title}>{data.description}</h2>
                    <NavLink className={style.link} to={`/product/${data.id}`}>Перейти</NavLink>
                </div>
            </Container>
        </section>
    );
}


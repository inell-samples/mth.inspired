import style from "./ProductSize.module.scss";
import cn from 'classnames';

export function ProductSize({ sizes, selectedSize, handleSizeChange }) {
    return (
        <div className={style.size}>
            <p className={style.title}>Размер</p>
            <div className={cn(style.list)}>
                {sizes?.map(sz => (
                    <label key={sz}>
                        <input className={style.input} type="radio" name="size"
                            value={sz} checked={selectedSize === sz}
                            onChange={handleSizeChange} />
                        <span className={cn(style.item, style.check)}>{sz}</span>
                    </label>)
                )}
            </div>
        </div>
    );
}
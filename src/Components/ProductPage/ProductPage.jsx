import { useEffect, useState } from 'react';
import { Container } from '../Layout/Container/Container';
import { useDispatch, useSelector } from 'react-redux';
import { fetchProduct } from '../../features/productSlice';
import { useParams } from 'react-router-dom';
import { API_URL } from '../../const';
import style from './ProductPage.module.scss';
import cn from 'classnames';
import { ColorList } from '../ColorList/ColorList';
import { Count } from '../Count/Count';
import { ProductSize } from './ProductSize/ProductSize';
import { fetchCategory } from '../../features/goodsSlice';
import { Goods } from '../Goods/Goods';
import { BtnLike } from '../BtnLike/BtnLike';
import { addToCart } from '../../features/cartSlice';

export function ProductPage() {

    const dispatch = useDispatch();
    const { id } = useParams();
    const { product } = useSelector(state => state.product);
    const { colorList } = useSelector(state => state.color);

    // Get fields from product using destructuration
    const { gender, category, colors } = product;

    const [count, setCount] = useState(1);
    const [selectedColor, setSelectedColor] = useState(null);
    const [selectedSize, setSelectedSise] = useState(null);

    const handleIncrement = () => {
        setCount(prevCount => ++prevCount)
    };

    const handleDecrement = () => {
        if (count > 1)
            setCount(prevCount => --prevCount);
    };

    const handleColorChange = (e) => {
        setSelectedColor(e.target.value);
    }

    const handleSizeChange = (e) => {
        setSelectedSise(e.target.value);
    }

    useEffect(() => {
        dispatch(fetchProduct(id));
    }, [id, dispatch]);

    useEffect(() => {
        dispatch(fetchCategory({
            gender,
            category,
            exclude: id,
            count: 4,
            top: true
        }));

    }, [gender, category, id, dispatch]);

    useEffect(() => {
        if (colorList?.length && colors?.length) {
            setSelectedColor(colorList.find(color => color.id === colors[0]).title);
        }
    }, [colorList, colors]);

    return (
        <>
            <section className={style.card}>
                <Container className={style.container}>
                    <img src={`${API_URL}${product.pic}`} alt={`${product.title}`} className={style.image} />
                    <form className={style.content} onSubmit={e => {
                        e.preventDefault();
                        dispatch(addToCart({
                            id,
                            color: selectedColor,
                            size: selectedSize ?? product?.size[0],
                            count
                        }));
                    }}>
                        <h2 className={style.title}>product.title</h2>

                        <p className={style.price}>руб {product.price}</p>

                        <div className={style.vendorCode}>
                            <span className={style.subtitle}>Артикул</span>
                            <span className={style.id}>{product.id}</span>
                        </div>

                        <div className={style.color}>
                            <p className={cn(style.subtitle, style.colorTitle)}>Цвет</p>
                            <ColorList colors={colors} selectedColor={selectedColor} handleColorChange={handleColorChange} />
                        </div>

                        <ProductSize sizes={product.size} selectedSize={selectedSize} handleSizeChange={handleSizeChange} />

                        <div className={style.description}>
                            <p className={cn(style.subtitle, style.descriptionTitle)}>Описание</p>
                            <p className={style.descriptionText}>{product.description}</p>
                        </div>

                        <div className={style.control}>
                            <Count className={style.count} count={count} handleIncrement={handleIncrement} handleDecrement={handleDecrement} />
                            <button className={style.addCart} tyle='submit'>В корзину</button>
                            <BtnLike id={id} />
                        </div>
                    </form>
                </Container>
            </section>

            <Goods title="Вам также может понравиться" />
        </>
    );
}
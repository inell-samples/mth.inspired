import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchAll } from "../../features/goodsSlice";
import { Goods } from "../Goods/Goods";
import style from './SearchPage.module.scss';
import { useSearchParams } from "react-router-dom";

export function SearchPage() {

    const { goodsList } = useSelector(state => state.goods);

    const dispatch = useDispatch();

    // Contaile search parameters for the request
    let [searchParams] = useSearchParams();

    useEffect(() => {
        const search = searchParams.get('q');

        const param = { search };
        dispatch(fetchAll(param));
    }, [searchParams, dispatch]);

    return (
        goodsList.length
            ? <Goods title="Результаты поиска" />
            : <h3 className={style.empty}>Ничего не найдено</h3>
    );
}
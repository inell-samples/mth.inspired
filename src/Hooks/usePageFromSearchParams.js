import { useEffect } from "react";
import { useLocation } from "react-router-dom";
import { setPage } from "../features/goodsSlice";

export function usePageFromSearchParams(dispath) {
    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const pageUrl = searchParams.get('page');

    useEffect(() => {
        dispath(setPage(pageUrl ?? 1));
    }, [dispath, pageUrl]);

    return pageUrl;
}
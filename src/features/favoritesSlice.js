import { createSlice } from "@reduxjs/toolkit";

const key = 'favorites';
const initialState = JSON.parse(localStorage.getItem(key) || '[]');

const favoritesSlice = createSlice({
    name: 'favorites',
    initialState,
    reducers: {
        addToFavorite(state, action) {
            const id = action.payload.id;
            if (!state.includes(id)) {
                state.push(id);
            }

            localStorage.setItem(key, JSON.stringify(state));
        },
        removeFromFavorite(state, action) {
            const id = action.payload.id;
            const index = state.indexOf(id);
            if (index !== -1) {
                state.splice(index, 1);
            }

            localStorage.setItem(key, JSON.stringify(state));
        }
    }
});

export const {addToFavorite, removeFromFavorite} = favoritesSlice.actions;

export default favoritesSlice.reducer;